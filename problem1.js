const fs = require("fs");
const path = require("path");

// 1. Create a directory of random JSON files
// 2. Delete those files simultaneously

const problem1 = (directory, callback) => {
  let totalFiles;

  const createDirectory = (directory, callback) => {
    fs.mkdir(path.join(__dirname + directory), { recursive: true }, (error) => {
      if (error) {
        callback(error);
      } else {
        callback(null, "Directory structure created\n");

        const fileList = Array(5)
          .fill(undefined)
          .map(() => {
            return `${~~(Math.random() * 10)}.json`;
          });
        totalFiles = fileList.length;
        createFiles(fileList);
      }
    });
  };

  const createFiles = (fileList) => {
    const errorFiles = [];
    const successFiles = [];

    for (let index = 0; index < fileList.length; index++) {
      createFile(fileList[index], errorFiles, successFiles);
    }
  };

  const createFile = (file, errorFiles, successFiles) => {
    fs.writeFile(
      path.join(__dirname + directory, file),
      "",
      { flag: "wx" },
      (error) => {
        if (error) {
          errorFiles.push(file);
          if (error.code === "EEXIST") {
            callback(`File Already Exists: ${error.path}`);
          } else {
            callback(error);
          }
        } else {
          successFiles.push(file);
          callback(null, `File Created: ${file}`);
        }
        if (errorFiles.length + successFiles.length === totalFiles) {
          callback(
            null,
            `\nAll files processed: \nErrors: ${errorFiles.length} \nSuccess: ${successFiles.length}`
          );

          callback(null, "\nDeleting all files Created:");
          for (let index = 0; index < successFiles.length; index++) {
            deleteFile(successFiles[index], callback);
          }
        }
      }
    );
  };

  const deleteFile = (file, callback) => {
    fs.unlink(path.join(__dirname, directory, file), (error) => {
      if (error) {
        callback(`File could not be deleted: ${error}`);
      } else {
        callback(null, `File successfully deleted: ${file}`);
      }
    });
  };
  createDirectory(directory, callback);
};

module.exports = problem1;
