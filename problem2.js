const fs = require("fs");
const readline = require("readline");

const problem2 = (file, filenamesFile) => {
  const readFile = (filepath, callback) => {
    fs.readFile(filepath, callback);
  };

  const writeFile = (filepath, data, callback) => {
    fs.writeFile(filepath, data, callback);
  };

  const appendFile = (filepath, data, callback) => {
    fs.appendFile(filepath, data, callback);
  };

  const getLineFromFileData = (data, lineNumber) => {
    return data.toString().split("\n")[lineNumber - 1];
  };

  const uppercase = (data) => {
    return data.toString().toString();
  };

  const lowercaseAndSplit = (data) => {
    return data.toString().toLowerCase().split(". ").join(".\n");
  };

  const sort = (data) => {
    return data.toString().split("\n").sort().join("\n");
  };

  const deleteAllFiles = (filenamesFile, callback) => {
    const readLine = readline.createInterface({
      input: fs.createReadStream(filenamesFile),
    });

    readLine.on("line", (filename) => {
      fs.unlink(filename, callback);
    });
  };

  const convertToUppercase = (error, data) => {
    writeFile("uppercase.txt", uppercase(data), (error, data) => {
      if (error) {
        console.trace(error);
      } else {
        writeFile(filenamesFile, `${filenamesFile}\nuppercase.txt`, (error) => {
          if (error) {
            console.trace(error);
          } else {
            convertToLowercase(2);
          }
        });
      }
    });
  };

  const convertToLowercase = (filenameLineNumberInFilenamesFile) => {
    readFile(filenamesFile, (error, data) => {
      if (error) {
        console.trace(error);
      } else {
        let filename = getLineFromFileData(
          data,
          filenameLineNumberInFilenamesFile
        );

        readFile(filename, (error, data) => {
          if (error) {
            console.trace(error);
          } else {
            writeFile(
              "lowercase-split.txt",
              lowercaseAndSplit(data),
              (error) => {
                if (error) {
                  console.trace(error);
                } else {
                  appendFile(
                    filenamesFile,
                    "\nlowercase-split.txt",
                    (error) => {
                      if (error) {
                        console.trace(error);
                      } else {
                        splitAndSort(3);
                      }
                    }
                  );
                }
              }
            );
          }
        });
      }
    });
  };

  const splitAndSort = (filenameLineNumberInFilenamesFile) => {
    readFile(filenamesFile, (error, data) => {
      if (error) {
        console.trace(error);
      } else {
        let filename = getLineFromFileData(
          data,
          filenameLineNumberInFilenamesFile
        );
        readFile(filename, (error, data) => {
          if (error) {
            console.trace(error);
          } else {
            writeFile("sort-lowercase.txt", sort(data), (error) => {
              if (error) {
                console.trace(error);
                return;
              }

              appendFile(filenamesFile, "\nsort-lowercase.txt", (error) => {
                if (error) {
                  console.trace(error);
                } else {
                  deleteAllFiles(filenamesFile, (error) => {
                    if (error) {
                      console.trace(error);
                    }
                  });
                }
              });
            });
          }
        });
      }
    });
  };

  readFile(file, (error, data) => {
    if (error) {
      console.log("File could not be read: ", error);
    } else {
      convertToUppercase(null, data);
    }
  });
};

module.exports = problem2;
